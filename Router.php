<?php
namespace Frost\Core;

class Router
{
	const REQUEST_GET	= 'GET';
	const REQUEST_POST	= 'POST';

	private $getRoutes; // [Route] => [ data, controller ]
	private $postRoutes;

	private static $currentRouteInfo = [ ];

	public function __construct()
	{
		$this->getRoutes	= [ ];
		$this->postRoutes 	= [ ];
	}

	/**
	 * Creates a GET request
	 * @param $route The URL route e.g. /user/:user_id
	 * @param string $controller The controller class name
	 * @param string $method The method to call in the controller
	 * @throws \Exception
	 */
	public function get($route, $controller, $method = '')
	{
		$parts = $this->getRouteParts($route);

		$this->getRoutes[$parts['route']] = [
			'getData'		=> $parts['arguments'],
			'controller'	=> $controller,
			'method' 		=> $method
		];
	}

	/**
	 * Creates a POST request
	 * @param string $route The URL route
	 * @param array $expectedPost The expected post keys
	 * @param string $controller The controller class name
	 * @param string $method The method to call in the controller
	 * @throws \Exception
	 */
	public function post($route, array $expectedPost, $controller, $method = '')
	{
		$parts = $this->getRouteParts($route);

		$this->postRoutes[$parts['route']]= [
			'getData'		=> $parts['arguments'],
			'postData' 		=> $expectedPost,
			'controller' 	=> $controller,
			'method'		=> $method
		];
	}

	/**
	 * Routes the URL to a controller
	 */
	public function route()
	{
		$url				= $_SERVER['REQUEST_URI'];
		$requestMethod		= $_SERVER['REQUEST_METHOD'];

		// Check the request type
		if($requestMethod == self::REQUEST_POST)
			$routes		= $this->postRoutes;
		else
			$routes		= $this->getRoutes;

		// Remove starting '/'
		if($url[0] == '/')
			$url = substr($url, 1);

		// Sort the route array by route length descending, so sub-routes can be found
		$keys = array_map('strlen', array_keys($routes));
		array_multisort($keys, SORT_DESC, $routes);

		foreach($routes as $route => $data)
		{
			if(substr($url, 0, strlen($route)) == $route)
			{
				$controller = new $data['controller']();

				// Check if the controller is valid
				if(!is_a($controller, '\Frost\Core\Controller'))
					throw new \Exception('The route controller is not a valid Frost controller');

				// Check what method to use
				if($requestMethod == self::REQUEST_GET)
					$controller->setData($this->getDataFromUrl($url, $data['getData']));
				else if($requestMethod == self::REQUEST_POST)
				{
					$allData = array_merge($this->getDataFromUrl($url, $data['getData']), $this->getValuesFromPost($data['postData']));
					$controller->setData($allData);
				}

				// Set the current route info
				self::$currentRouteInfo = array_values(array_filter(explode('/', $route)));

				$controller->setRoute($route);

				// Check if there is a method defined
				if(isset($data['method']))
				{
					$method = $data['method'];
					$controller->$method();
				}
				else
					$controller->default();

				return;
			}
		}

		// If no route is loaded, load the wildcard (/*)
		if(in_array('/*/', array_keys($routes)))
		{
			self::$currentRouteInfo = array_values(array_filter(explode('/', $route)));

			$controller = $this->routes['/*/']['controller'];
			$controller->setRoute('*'.$url);
			$controller->default();
		}
	}

	/**
	 * Gets the route parts from a string. Each part is seperated with a '/', arguments begin with ':'.
	 * @param type $route The route to get parts from
	 * @return type An array containing the resource & the arguments
	 */
	private function getRouteParts($route)
	{
		$parts = explode('/', $route);

		// All variables are at end of route
		if(!strpos($route, ':'))
			$routePath = $route;
		else
			$routePath = substr($route, 0, strpos($route, ':'));

		// Get all arguments from the route
		$args = array_map(function($part) {
			// If its not a argument, leave it blank
			if(empty($part))
				return;

			if($part[0] == ':')
				return substr($part, 1);
		}, $parts);

		// Remove empty values
		$args = array_filter($args);

		// Change the index to the name of the argument
		foreach($args as $index => $value)
		{
			$args[substr($parts[$index], 1)] = [ 'index' => $index, 'value' => $value ];
			unset($args[$index]);
		}

		return [
			'route'		=> $routePath,
			'arguments'	=> $args
		];
	}

	/**
	 * Gets the route data from a URL
	 * @param string $url The URL to use
	 * @param array $expectedArgs An array of expected data
	 * @return type An array of arguments, with the argument name as the key
	 */
	private function getDataFromUrl($url, array $expectedArgs)
	{
		$args 		= [ ];
		$parts		= explode('/', $url);

		foreach($expectedArgs as $key => $value)
		{
			if(isset($parts[$value['index']]))
				$args[$key] = $parts[$value['index']];
		}

		return filter_var_array($args, FILTER_SANITIZE_STRING);
	}

	/**
	 * Gets the allowed values from POST
	 * @param array $allowed An array of allowed POST keys
	 * @return array An array of POST items
	 */
	private function getValuesFromPost(array $allowed)
	{
		$_POST	= filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

		$args	= [ ];

		if(empty($_POST) || empty($allowed))
			return $args;

		foreach($allowed as $key => $value)
		{
			if(!isset($_POST[$value]))
				$args[$value] = '';
			else
				$args[$value] = $_POST[$value];
		}

		$_POST = [ ];

		return $args;
	}

	/**
	 * Gets the current route info
	 * @return array
	 */
	public static function getCurrentRouteInfo()
	{
		return self::$currentRouteInfo;
	}

	public static function camelCase($string)
	{
		$string = str_replace([ '-', '_' ], ' ', $string);
		$string = ucwords($string);
		$string = str_replace(' ', '', $string);
		return lcfirst($string);
	}
}
