<?php
namespace Frost\Core;

class Config
{
	private static $config = [ ];

	private function __construct()
	{

	}

	public static function readConfig($name = 'frost', $configFile = 'frost.json')
	{
		if(!file_exists(CONFIG_ROOT.$configFile))
			throw new \Exception("Cannot find config: '{$configFile}'");

		$configTxt = file_get_contents(CONFIG_ROOT.$configFile);

		self::$config[$name] = json_decode($configTxt, true);
	}

	public static function getSetting($name, $from = 'frost')
	{
		if(!isset(self::$config[$from]) || !isset(self::$config[$from][$name]))
			return false;

		return self::$config[$from][$name];
	}
}
