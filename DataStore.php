<?php
/**
 * Class for storing data, intended to be used with templates
 *
 * User: antony
 * Date: 04/10/2018
 * Time: 23:13
 */

namespace Frost\Core;

class DataStore
{
	private $data = [ ];

	/**
	 * Adds a data to the store
	 * @param $key
	 * @param $data
	 */
	public function add($key, $data)
	{
		$this->data[$key] = $data;
	}

	/**
	 * Gets a data value
	 * @param $key string The data key
	 * @return mixed|string
	 */
	public function get($key)
	{
		return isset($this->data[$key]) ? $this->data[$key] : '';
	}

	/**
	 * Echos a data value
	 * @param $key string The data key
	 */
	public function show($key)
	{
		echo $this->get($key);
	}

	/**
	 * Checks if the data store has a key, if so return $then otherwise $else
	 * @param string $key The key to look for
	 * @param string $then The data to return if true
	 * @param string $else The data to return if false
	 * @return string
	 */
	public function has($key, $then, $else = '')
	{
		return isset($this->data[$key]) && !empty($this->data[$key]) ? $then : $else;
	}
}