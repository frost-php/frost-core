<?php
namespace Frost\Core;

session_start();

// Declare
define('FROST_VERSION',	0.1);
define('BASE_ROOT', 	$_SERVER['DOCUMENT_ROOT'].'/');
define('FROST_ROOT', 	$_SERVER['DOCUMENT_ROOT']."/Frost/");
define('APP_ROOT', 		$_SERVER['DOCUMENT_ROOT']."/App/");
define('CONFIG_ROOT', 	$_SERVER['DOCUMENT_ROOT']."/cfg/");
define('TEMPLATE_ROOT', $_SERVER['DOCUMENT_ROOT'].'/Templates');

require_once('Autoload.php');

Config::readConfig();
