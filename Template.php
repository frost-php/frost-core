<?php
namespace Frost\Core;

class Template
{
	private $parent;
	private $child;

	public function __construct()
	{
		$this->parent 	= '';
		$this->child 	= '';
	}

	/**
	 * @param $template
	 * @throws \Exception
	 */
	public function parent($template)
	{
		if(!empty($this->parent))
			throw new \Exception('Template already has a parent');

		$this->parent = $template;
	}

	/**
	 * Includes another template into the current template
	 * @param string $template The template name
	 */
	public function include($template)
	{
		echo (new Template())->load($template);
	}

	/**
	 * Sets the content HTML used by a parent
	 * @param string $html The child HTML
	 */
	public function setChildContent($html)
	{
		$this->child = $html;
	}

	/**
	 * Echos the child HTML
	 */
	public function getChild()
	{
		echo $this->child;
	}

	/**
	 * Loads a template from file
	 * @param string $template The template name
	 * @param DataStore $data The data store object
	 * @param string $templateRoot The template root for the view
	 * @return string
	 */
	public function load($template, DataStore $data, $templateRoot = TEMPLATE_ROOT)
	{
		ob_start();
		include $templateRoot.'/'.$template.'.php';
		$content = ob_get_clean();

		$parent = '';
		// If there is a parent, load it
		if(!empty($this->parent))
		{
			$parent = new Template();
			$parent->setChildContent($content);
			return $parent->load($this->parent, $data, $templateRoot);
		}
		else
			return $content;
	}
}
