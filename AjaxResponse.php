<?php
namespace Frost\Core;

class AjaxResponse
{
	private $data;

	/**
	 * Sends a AjaxResponse
	 * @param       $code The response code
	 * @param array $data The response data
	 */
	public function __construct($code, array $data = [ ])
	{
		$this->data 						= [
			'code' 	=> $code,
			'data'	=> $data
		];
	}

	/**
	 * Adds data to the response
	 * @param array $data The data to add
	 */
	public function addData(array $data)
	{
		$this->data['data'] 				= array_merge($this->data['data'], $data);
	}

	/**
	 * Send the response
	 * @param bool $terminate True to terminate script after send
	 */
	public function send($terminate = false)
	{
		http_response_code($this->data['code']);
		echo json_encode($this->data);
		if($terminate)
			die();
	}
}