<?php
namespace Frost\Core;

/**
 * The Frost database class
 */
class Db
{
	/**
	 * DB return codes
	 *
	 * @var int
	 */
	const DB_OK		= 1;
	const DB_YES		= 1;
	const DB_NO		= 0;
	const DB_ERROR		= -1;
	const DB_ERROR_SQL	= -2;
	/**#@-*/

	private $db_conn; // The database connection

	/**
	 * Create a connection to the database
	 */
	public function __construct()
	{
		try
		{
			$host		= Config::getSetting('DB_HOST');
			$name		= Config::getSetting('DB_NAME');
			$username	= Config::getSetting('DB_USERNAME');
			$password	= Config::getSetting('DB_PASSWORD');

			$this->db_conn 	= new \PDO("mysql:host={$host};dbname={$name};charset=utf8", $username, $password, [ \PDO::ATTR_EMULATE_PREPARES => true, \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION, \PDO::ATTR_PERSISTENT => true ]);
		}
		catch(\PDOException $e)
		{
			throw new \Exception('Failed to connect to database.'.$e->getMessage);
		}
	}

	// --------------------------
	// 		Database functions
	// --------------------------

	/**
	 * Enable the use of PDO transactions
	 */
	public function useTransaction()
	{
		$this->db_conn->beginTransaction();
	}

	/**
	 * Commits any made changes when using transactions
	 */
	public function commit()
	{
		$this->db_conn->commit();
	}

	/**
	 * Rollback any changes made when using transactions
	 */
	public function rollback()
	{
		$this->db_conn->rollBack();
	}

	/**
	 * Gets the last inserted ID
	 * @return int The last inserted ID
	 */
	public function getLastInsertId()
	{
		return $this->db_conn->lastInsertId();
	}

	/**
	 * Checks if a item is in the database.
	 *
	 * @param string $table The name of the table to check
	 * @param string $col The column to check
	 * @param string $key The value to check
	 *
	 * @return int Returns a DB error code on completion
	 */
	public function isInDatabase($table, $col, $key)
	{
		$q = $this->prepare("SELECT $col FROM $table WHERE $col = :key");

		if(!$this->execute($q, [ 'key' => $key ]))
			return self::DB_ERROR;
		else
			return ($q->rowCount() == 0) ? self::DB_NO : self::DB_YES;
	}

	/**
	 * Run a SQL query against the database.
	 *
	 * @param string $sql The SQL query to execute.
	 *
	 * @return PDOStatement If succeded returns a PDOStatement, otherwise null.
	 */
	public function query($sql)
	{
		return $this->db_conn->query($sql);
	}

	/**
	 * Prepare an SQL statement.
	 *
	 * @param string $sql the SQL to prepare
	 *
	 * @return PDOStatement
	 */
	public function prepare($sql)
	{
		return $this->db_conn->prepare($sql);
	}

	/**
	 * Binds a value to a prepared statement.
	 *
	 * @param \PDOStatement $statement The prepared statement to use
	 * @param mixed $paramId The paramater identifier
	 * @param mixed $value The value to bind
	 * @param int $type The data type of the value
	 */
	public function bind(\PDOStatement $statement, $paramId, $value, $type = \PDO::PARAM_STRING)
	{
		$statement->bindParam($paramId, $value, $type);
	}

	/**
	 * Executes a prepared statement.
	 *
	 * @param \PDOStatement $statement The prepared statement to execute
	 * @param array $parameters Values to bind
	 *
	 * @return bool True on completeion otherwise false
	 */
	public function execute(\PDOStatement $statement, array $parameters = null)
	{
		return $statement->execute($parameters);
	}

	/**
	 * Prepares and executes a SQL statement.
	 * @param string $sql The SQL to prepare
	 * @param array $parameters Values to bind
	 * @return mixed Executed query if success, otherwise false
	 */
	public function prepareExecute($sql, array $parameters = null)
	{
		$statement	= $this->prepare($sql);
		$result		= $statement->execute($parameters);

		if($result != false)
			return $statement;
		else
			return self::DB_ERROR;
	}
}
