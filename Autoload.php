<?php
namespace Frost\Core;

spl_autoload_register(function($class) {
	$filePath = str_replace('\\', DIRECTORY_SEPARATOR, $class);

	if(file_exists(BASE_ROOT.$filePath.'.php'))
		require_once(BASE_ROOT.$filePath.'.php');
});
