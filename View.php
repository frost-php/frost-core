<?php
namespace Frost\Core;

class View
{
	private $externalCssSheets;
	private $externalJavaScript;
	private $internalCss;
	private $internalJavascript;

	private $content;
	protected $data;

	/**
	 * View constructor.
	 */
	public function __construct()
	{
		$this->externalCssSheets 	= [ ];
		$this->externalJavaScript 	= [ ];
		$this->internalCss 		= [ ];
		$this->internalJavascript 	= [ ];

		$this->content 			= '';
		$this->data 			= [ ];
	}

	/**
	 * Add a CSS file to the page
	 * @param string $css The href to the stylesheet
	 * @param bool $external The URL is external to the system
	 */
	public function addCss($css, $external = false)
	{
		if(!$external)
			$url = '/public/'.$css;
		else
			$url = $css;

		$this->externalCssSheets[] = "<link type='text/css' rel='stylesheet' href='{$url}' />";
	}

	/**
	 * Add a JS file to the page
	 * @param string $js The href to the script file
	 * @param bool $external The URL is external to the system
	 */
	public function addJavaScript($js, $external = false)
	{
		if(!$external)
			$url = '/public/'.$js;
		else
			$url = $js;

		$this->externalJavaScript[] = "<script type='text/javascript' src='{$url}'></script>";
	}

	/**
	 * Adds instyle CSS to the page
	 * @param string $css The css to add
	 */
	public function addInternalCss($css)
	{
		$this->internalCss[] = '<style type="text/css">'.$css.'</style>';
	}

	/**
	 * Adds instyle JavaScript to the page
	 * @param string $js The JavaScript to add
	 */
	public function addInternalJavascript($js)
	{
		$this->internalJavascript[] = '<script type="text/javascript">'.$js.'</script>';
	}

	/**
	 * Loads a template
	 * @param string $template The template name
	 * @param DataStore $data The data store object
	 * @param string $templateRoot The template root for the view
	 */
	public function load($template, ?DataStore $data, $templateRoot = TEMPLATE_ROOT)
	{
		$data = $data == null ? new DataStore() : $data;

		$data->add('header-css', implode('',  array_merge($this->externalCssSheets, $this->internalCss)));
		$data->add('header-js', implode('', array_merge($this->externalJavaScript, $this->internalJavascript)));

		$this->content = (new Template())->load($template, $data, $templateRoot);
	}

	/**
	 * Echos the view template
	 */
	public function print()
	{
		echo $this->content;
		die();
	}
}
